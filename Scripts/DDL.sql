use EasyHotel;
Drop table if exists  `User` ;
CREATE TABLE `User` (
  `UserId` INT NOT NULL AUTO_INCREMENT Primary Key,
  `UserName` varchar(20) ,
  `PassWord` varchar(200),
  `FullName` varchar(50) ,
   GroupHotelId int,
  `SubHotelId` int,
   RoleId int,
  `Created` datetime DEFAULT now(),
   Modified datetime DEFAULT now()
) ;

Drop table if exists  `Role` ;
CREATE TABLE `Role` (
  `RoleId` INT NOT NULL AUTO_INCREMENT Primary Key,
  `Name` varchar(20) DEFAULT NULL,
  `Created` datetime DEFAULT now(),
   Modified datetime DEFAULT now()
) ;


Drop table if exists  `GroupHotel` ;
CREATE TABLE `GroupHotel` (
  `GroupHotelId` INT NOT NULL AUTO_INCREMENT Primary Key,
  `Name` varchar(100) DEFAULT NULL,
  `Description`   varchar(1000),
  `Created` datetime DEFAULT now(),
   Modified datetime DEFAULT now()
);


Drop table if exists  `SubHotel` ;
CREATE TABLE `SubHotel` (
  `SubHotelId` INT NOT NULL AUTO_INCREMENT Primary Key,
  `GroupHotelId` INT,
  `Name` varchar(200) ,
   Fax varchar(500),
   Phone varchar(500),
   Address VARCHAR(400),
  `Created` datetime DEFAULT now(),
   Modified datetime DEFAULT now()
);

Drop table if exists  `Room` ;
CREATE TABLE `Room` (
  `RoomId` INT NOT NULL AUTO_INCREMENT Primary Key,
  `RoomNo` varchar(20) ,
  `RoomTypeId` int ,
  `RoomStatusId` int,
  `SubHotelId` int,
  `Created` datetime DEFAULT now(),
   Modified datetime DEFAULT now()
);

CREATE INDEX `idx_Room_RoomNo`  ON `Room` (RoomNo) ;
CREATE INDEX `idx_Room_SubHotelId`  ON `Room` (SubHotelId) ;

Drop table if exists  `RoomType` ;
CREATE TABLE `RoomType` (
  `RoomTypeId` INT NOT NULL AUTO_INCREMENT Primary Key,
  `Name` varchar(50) ,
  `SinglePrice` DECIMAL(7,2) NULL DEFAULT 0,
  `MinFeeForHourRoom` DECIMAL(7,2) NULL DEFAULT 60,
  `PricePerHour` DECIMAL(7,2),
   GroupHotelId int,
  `Created` datetime DEFAULT now(),
   Modified datetime DEFAULT now()
) ;

Drop table if exists  `TravelAgency` ;
CREATE TABLE `TravelAgency` (
  `TravelAgencyId` INT NOT NULL AUTO_INCREMENT Primary Key,
  `Name` varchar(50) ,
  `ContactName` varchar(50) ,
  `ContactPhone` varchar(20) ,
   GroupHotelId int,
  `Created` datetime DEFAULT now(),
   Modified datetime DEFAULT now()
);

Drop table if exists  `Bill` ;
CREATE TABLE `Bill` (
  `BillId` bigint NOT NULL AUTO_INCREMENT Primary Key,
  `IsHourRoom` TINYINT NULL DEFAULT 0
   BillNo varchar(50),
   RoomId  int,
   CheckinDate datetime,
   CheckoutDate datetime,
   StayTotalDays int,
   StayTotalHours int,
   SinglePrice decimal(7,2) default 0,
   MinFeeForHourRoom decimal(7,2) default 0,
   PricePerHour decimal(7,2) default 0,
   TotalFee decimal(12,2) default 0,
   `ConsumeFee` decimal(12,2) default 0,
   `ConsumeItem` varchar(1000),
   Deposit decimal(12,2),
   TravelAgencyId int,
   CustomerName varchar(50),
   CustomerIDNo varchar(50),
   CreatedByUserId int,
   UpdatedByUserId int,
   PayTypeId int,
   BillStatus int,
   Comments varchar(1000),
  `Created` datetime DEFAULT now(),
   Modified datetime DEFAULT now()
);
CREATE INDEX `idx_bill_RoomId`  ON `bill` (RoomId) ;
CREATE INDEX `idx_bill_CheckinDate`  ON `bill` (CheckinDate) ;
CREATE INDEX `idx_bill_CheckOutDate`  ON `bill` (CheckOutDate) ;

Drop table if exists  `PayType` ;
CREATE TABLE `PayType` (
  `PayTypeId` int NOT NULL AUTO_INCREMENT Primary Key,
   Name varchar(20),
  `Created` datetime DEFAULT now(),
   Modified datetime DEFAULT now()
);

drop table if exists SiteLog;
create table if not exists
    SiteLog (SiteLogId bigint auto_increment primary key,
		RequestId      varchar(50),
		RequestUrl      varchar(8000),
		UserId        int,
		UserAgent   varchar(500),
		ClientDeviceId      varchar(100),
		ClientIP      varchar(100),
		RequestBeginTime      varchar(50),
		RequestEndTime      varchar(50),
		Elapsed float,
		`LogLevel`        varchar(10),
		LoggerName         varchar(200),
		`Message`      LONGTEXT,
		Exeption  LONGTEXT,
		Request LONGTEXT,
		Response LONGTEXT,
		Created datetime);

