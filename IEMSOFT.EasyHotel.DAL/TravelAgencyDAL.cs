﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IEMSOFT.EasyHotel.DAL.Models;
using IEMSOFT.EasyHotel.Common;
namespace IEMSOFT.EasyHotel.DAL
{
    public class TravelAgencyDAL
    {
        private IDBProvider _dbProvider;
        public TravelAgencyDAL(IDBProvider dbProvider)
        {
            _dbProvider = dbProvider;
        }

        public List<TravelAgency> GetList(int groupHotelId)
        {
            var sql = new StringBuilder();
            sql.Append("select * from TravelAgency ");
            sql.Append("where GroupHotelId=@0");
            var ret = _dbProvider.DB.Fetch<TravelAgency>(sql.ToString(),groupHotelId);
            return ret;
        }


        public TravelAgency GetOne(int travelAgencyId)
        {
            var sql = new StringBuilder();
            sql.Append("select * from TravelAgency ");
            sql.Append("where TravelAgencyId=@0");
            var ret = _dbProvider.DB.FirstOrDefault<TravelAgency>(sql.ToString(), travelAgencyId);
            return ret;
        }


        public void Save(TravelAgency travelAgency)
        {
            var result = GetOneByName(travelAgency.Name,travelAgency.GroupHotelId);
            if (result == null)
            {
                _dbProvider.DB.Insert(travelAgency);
            }
            else
            {
                travelAgency.Modified = DateTime.Now;
                travelAgency.TravelAgencyId = result.TravelAgencyId;
                travelAgency.Created = result.Created;
                _dbProvider.DB.Update(travelAgency);
            }
        }


        public void Add(TravelAgency travelAgency)
        {
            _dbProvider.DB.Insert(travelAgency);
        }

        public TravelAgency GetOneByName(string tavelAgencyName, int groupHotelId)
        {
            var ret = _dbProvider.DB.FirstOrDefault<TravelAgency>("select * from TravelAgency where Name=@0 and GroupHotelId=@1", 
                                                                  tavelAgencyName, groupHotelId);
            return ret;
        }

        public void Remove(List<TravelAgency> rooms)
        {
            foreach (var item in rooms)
            {
                Remove(item);
            }
        }

        public void Remove(TravelAgency room)
        {
            _dbProvider.DB.Delete(room);
        }
    }
}
