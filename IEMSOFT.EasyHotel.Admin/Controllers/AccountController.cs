﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using IEMSOFT.EasyHotel.Admin.Models;
using IEMSOFT.EasyHotel.Common;
using IEMSOFT.EasyHotel.DAL;
using IEMSOFT.EasyHotel.DAL.Models;
using IEMSOFT.Foundation;
using IEMSOFT.Foundation.MVC;
namespace IEMSOFT.EasyHotel.Admin.Controllers
{
    public class AccountController : BaseController
    {

        public ActionResult Index()
        {
            if (CurrentUser.RoleId == RoleType.GroupLeader.ToInt())
            {
                ViewBag.RoleIdForCombobox = RoleType.HotelManager.ToInt();
            }
            else if (CurrentUser.RoleId == RoleType.HotelManager.ToInt())
            {
                ViewBag.RoleIdForCombobox = RoleType.Reception.ToInt();
            }
            ViewBag.CurrentUser = CurrentUser;
            return View();
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = Server.UrlEncode(returnUrl);
            return View();
        }

        [AllowAnonymous]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return View("login");
        }

        [HttpPost]
        public ActionResult UpdatePassword(UpdatePasswordModel model)
        {
            var ret = new BasicResult<string, List<string>>();
            ret.Msg = new List<string>();
            if (model.NewPassword != model.VerifyNewPassword)
            {
                ret.Msg.Add("新密码与确认密码不匹配！");
            }
            else
            {
                using (var db = new EasyHotelDB())
                {
                    var userDAL = new UserDAL(db);
                    var user = userDAL.GetOneById(CurrentUser.UserId);
                    if (user.PassWord.DESDecrypt() != model.OldPassword)
                    {
                        ret.Msg.Add("旧密码不正确！");
                    }
                    else
                    {
                        userDAL.UpdatePassword(CurrentUser.UserId, model.NewPassword.DESEncrypt());
                    }
                }
            }

            return JsonNet(ret);
        }

        [HttpGet]
        public ActionResult UpdatePassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginModel model)
        {
            var ret = new BasicResult<string, List<string>>();
            ret.Msg = new List<string>();
            using (var db = new EasyHotelDB())
            {
                var userDAL = new UserDAL(db);
                var user = userDAL.GetOne(model.GroupHotelId, model.UserName, model.RoleId);
                if (user == null)
                {
                    ret.Msg.Add("该用户不属于该酒店集团或者该职务！");
                }
                else
                {
                    if (user.PassWord.DESDecrypt() != model.Password)
                    {
                        ret.Msg.Add("密码不正确！");
                    }
                    else
                    {
                        var CurrentUser = AutoMapper.Mapper.Map<UserModel>(user);
                        var jsonStr = Newtonsoft.Json.JsonConvert.SerializeObject(CurrentUser);
                        jsonStr = jsonStr.DESEncrypt();
                        FormsAuthentication.SetAuthCookie(jsonStr, true);
                    }
                }
            }
            return JsonNet(ret);

        }

        public ActionResult Get()
        {
            using (var db = new EasyHotelDB())
            {
                var dal = new UserDAL(db);
                var users = dal.GetList(CurrentUser.GroupHotelId, CurrentUser.SubHotelId, CurrentUser.RoleId);
                var model = AutoMapper.Mapper.Map<List<UserFullModel>>(users);
                return JsonNet(model);
            }
        }

        [HttpPost]
        public ActionResult Add(UserFullModel model)
        {
            var ret = new BasicResult<string, List<string>>();
            ret.Msg = new List<string>();
            using (var db = new EasyHotelDB())
            {
                var dal = new UserDAL(db);
                var existOne = dal.GetOne(CurrentUser.GroupHotelId, model.UserName, model.RoleId);
                if (existOne != null)
                {
                    ret.Msg.Add("该用户名已经在该集团酒店中存在!");
                }
                else
                {
                    model.GroupHotelId = CurrentUser.GroupHotelId;
                    if (CurrentUser.RoleId == RoleType.HotelManager.ToInt()) //店长操作用户管理时
                    {
                        model.SubHotelId = CurrentUser.SubHotelId;
                    }
                    var user = AutoMapper.Mapper.Map<User>(model);
                    dal.Add(user);
                }
            }
            return JsonNet(ret);
        }

        [HttpPost]
        public ActionResult update(UserFullModel model)
        {
            var ret = new BasicResult<string, List<string>>();
            ret.Msg = new List<string>();
            using (var db = new EasyHotelDB())
            {
                var dal = new UserDAL(db);
                var dalModel = AutoMapper.Mapper.Map<User>(model);
                dalModel.GroupHotelId = CurrentUser.GroupHotelId; //将酒店集团ID自动设置成与该用户一致
                if (CurrentUser.RoleId == RoleType.HotelManager.ToInt()) //店长操作用户管理时
                {
                    dalModel.SubHotelId = CurrentUser.SubHotelId;
                }
                dal.Update(dalModel);
            }
            return JsonNet(ret);
        }

        [HttpPost]
        public ActionResult Remove([FromJson]List<UserFullModel> model)
        {
            var ret = new BasicResult<string, List<string>>();
            ret.Msg = new List<string>();
            using (var db = new EasyHotelDB(true))
            {
                using (var tx = db.DB.GetTransaction())
                {
                    var dal = new UserDAL(db);
                    foreach (var item in model)
                    {
                        dal.RemoveById(item.UserId);
                    }
                    tx.Complete();
                }
            }
            return JsonNet(ret);
        }
    }
}
