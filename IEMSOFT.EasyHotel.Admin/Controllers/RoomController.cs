﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMSOFT.EasyHotel.DAL;
using IEMSOFT.EasyHotel.DAL.Models;
using IEMSOFT.EasyHotel.Common;
using IEMSOFT.EasyHotel.Admin.Models;
using IEMSOFT.Foundation.MVC;
using IEMSOFT.EasyHotel.Admin.Lib;
using IEMSOFT.Foundation;
using System.Text;
namespace IEMSOFT.EasyHotel.Admin.Controllers
{
    public class RoomController : BaseController
    {
        //
        // GET: /RoomType/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RoomStatus(int? subHotelId)
        {
            if (subHotelId == null)
            {
                subHotelId = 0;
            }
            if (CurrentUser.RoleId != RoleType.GroupLeader.ToInt())
            {
                subHotelId = CurrentUser.SubHotelId;
            }
            else//如果是集团领导
            {
                if (subHotelId != 0)
                {
                    var subHotels = SubHotelBLL.GetSubHotelList(CurrentUser.GroupHotelId);//
                    if (subHotels.Count>0&&!subHotels.Exists(item => item.SubHotelId == subHotelId))
                    {
                        subHotelId = subHotels[0].SubHotelId;
                    }
                }
            }
            ViewBag.CurrentUser = CurrentUser;
            ViewBag.RoomColumnCount = Utility.GetAppSetting<int>(8, "RoomColumnCount");
            var roomStatus = RoomBLL.GetRoomStatus(DateTime.Now, subHotelId.Value);
            ViewBag.SubHotelId = subHotelId;
            return View(roomStatus);
        }

        public ActionResult RoomStatusOption(int roomStatusId)
        {
            var ret = RoomBLL.GetRoomStatusOption(DateTime.Now, CurrentUser.SubHotelId, roomStatusId.ToEnum<RoomStatusType>());
            return JsonNet(ret);
        }
        public ActionResult Get()
        {
            var ret = RoomBLL.GetRooms(CurrentUser.SubHotelId);
            return JsonNet(ret);
        }

        [HttpPost]
        public ActionResult Add(RoomAddModel model)
        {
            var ret = new BasicResult<string, List<string>>();
            var rooms = new List<Room>();

            ret.Msg = new List<string>();
            using (var db = new EasyHotelDB(true))
            {
                for (var i = 0; i < model.RoomCount; i++)
                {
                    var room = new Room()
                    {
                        RoomNo = (model.FromRoomNo + i).ToString(),
                        IsMending = false,
                        RoomTypeId = model.RoomTypeId,
                        SubHotelId = CurrentUser.SubHotelId
                    };
                    rooms.Add(room);
                }
                var existRooms = RoomBLL.GetRooms(CurrentUser.SubHotelId);

                var checkResults = (from existRoom in existRooms
                                    join room in rooms on existRoom.RoomNo equals room.RoomNo
                                    select existRoom).ToList();

                if (checkResults.Count > 0)
                {
                    var strBuilder = new StringBuilder();
                    strBuilder.Append("这些房间号已经存在：");
                    foreach (var item in checkResults)
                    {
                        strBuilder.Append(string.Format("{0}，", item.RoomNo));
                    }
                    ret.Msg.Add(strBuilder.ToString());
                }
                else
                {

                    using (var tx = db.DB.GetTransaction())
                    {
                        var dal = new RoomDAL(db);
                        foreach (var room in rooms)
                        {
                            dal.Add(room);
                        }
                        tx.Complete();
                    }
                }
            }
            return JsonNet(ret);
        }

        [HttpPost]
        public ActionResult Update(RoomModel model)
        {
            var ret = new BasicResult<string, List<string>>();
            ret.Msg = new List<string>();
            using (var db = new EasyHotelDB())
            {
                var dal = new RoomDAL(db);
                dal.Update(model.RoomId, model.RoomTypeId);
            }
            return JsonNet(ret);
        }

        [HttpPost]
        public ActionResult SetMending(int roomId, int isMending)
        {
            var ret = new BasicResult<string, List<string>>();
            ret.Msg = new List<string>();
            RoomBLL.SetMending(roomId, isMending.ToBool());
            return JsonNet(ret);
        }
        [HttpPost]
        public ActionResult Remove([FromJson]List<RoomModel> models)
        {
            var ret = new BasicResult<string, List<string>>();
            ret.Msg = new List<string>();
            using (var db = new EasyHotelDB(true))
            {
                using (var tx = db.DB.GetTransaction())
                {
                    var dal = new RoomDAL(db);
                    foreach (var item in models)
                    {
                        dal.Remove(item.RoomId);
                    }
                    tx.Complete();
                }
            }
            return JsonNet(ret);
        }

        [HttpPost]
        public ActionResult ReviseCheckin(BillBasicModel model)
        {
            var ret = new BasicResult<string, List<string>>();
            ret.Msg = new List<string>();
            model.UpdatedByUserId = CurrentUser.UserId;
            BillBLL.Update(model);
            return JsonNet(ret);
        }

        [HttpPost]
        public ActionResult CancelCheckin(int roomId)
        {
            var ret = new BasicResult<string, List<string>>();
            ret.Msg = new List<string>();
            BillBLL.CancelCheckin(roomId, CurrentUser.UserId);
            return JsonNet(ret);
        }

        [HttpPost]
        public ActionResult PrintCheckin(BillCreateModel model)
        {
            if (model.BillId <= 0)//新增订单
                model.CreatedByUserId = CurrentUser.UserId;
            var printCheckinModel = RoomBLL.GetPrintCheckinModel(model);
            return PartialView("PrintCheckin", printCheckinModel);
        }

        [HttpPost]
        public ActionResult PrintCheckout(BillModel model)
        {
            if (model.BillId <= 0)//新增订单
                model.CreatedByUserId = CurrentUser.UserId;
            var printCheckinModel = RoomBLL.GetPrintCheckoutModel(model);
            return PartialView("PrintCheckout", printCheckinModel);
        }

        [HttpGet]
        public ActionResult CheckinStatus()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SearchCheckinStatus(int? subHotelId)
        {
            var ret = new BasicResult<List<RoomStatusModel>, List<string>>();
            ret.Data = new List<RoomStatusModel>();
            ret.Msg = new List<string>();
            if (subHotelId == null)
            {
                var subhotels = SubHotelBLL.GetSubHotelList(CurrentUser.GroupHotelId);
                foreach (var subHotel in subhotels)
                {
                    var rooms = RoomBLL.GetRoomStatus(DateTime.Now, subHotel.SubHotelId);
                    ret.Data.AddRange(rooms);
                }
            }
            else
            {
                var rooms = RoomBLL.GetRoomStatus(DateTime.Now, subHotelId.Value);
                ret.Data.AddRange(rooms);
            }
            return JsonNet(ret);
        }
    }
}
