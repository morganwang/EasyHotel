﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMSOFT.EasyHotel.DAL;
using IEMSOFT.EasyHotel.DAL.Models;
using IEMSOFT.EasyHotel.Common;
using IEMSOFT.EasyHotel.Admin.Lib;
using IEMSOFT.Foundation;
namespace IEMSOFT.EasyHotel.Admin.Controllers
{
    public class RoleController : BaseController
    {
        //
        // GET: /Role/
        [AllowAnonymous]
        public ActionResult Option()
        {

            var options = RoleBLL.GetRoles();
            if (options.Count >=1)
            {
                options[0].Selected = true;
            }
            return JsonNet(options);
        }

        public ActionResult FilterOption()
        {

            var options = RoleBLL.GetRoles();
            options.RemoveAll(item => item.Value.ToInt() > CurrentUser.RoleId);
            return JsonNet(options);
        }
    }
}
