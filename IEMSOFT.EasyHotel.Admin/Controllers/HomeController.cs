﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMSOFT.EasyHotel.Admin.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index(string returnUrl)
        {
            ViewBag.CurrentUser = CurrentUser;
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }
    }
}
