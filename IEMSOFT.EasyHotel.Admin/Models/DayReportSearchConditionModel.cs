﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMSOFT.EasyHotel.Admin.Models
{
    public class DayReportSearchConditionModel
    {
        public string  FromDate { get; set; }
        public string EndDate { get; set; }
        public int? RoomTypeId { get; set; }
        public int? TravelAgencyId { get; set; }
        public int? PayTypeId { get; set; }
        public int? SubHotelId { get; set; }
    }
}